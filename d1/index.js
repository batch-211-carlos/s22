console.log("Hello World")

//Array methods
//Javascript has built in functions and methods for Arrays. This allows us to manipulate and access our array items

//Mutator methods

/*
		-mutator methods are functions that "mutate" or change our array after they are created
		- this methods manipulate the original array performing various tasks such as adding and removing elements
*/

		let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

		//push()
		/*
		-adds an element in the end of an array AND returns the array's length

		Syntax:
				arrayName.push();
		*/

		console.log('Current Array: ');
		console.log(fruits); //log fruits array
		let fruitsLength = fruits.push('Mango'); //5
		console.log(fruitsLength);
		console.log('Mutated array from push method: ');
		console.log(fruits); //Mango is added at the end of the Array

		fruits.push('Avocado', 'Guava'); //multiple
		console.log('Mutated array from push method: ');
		console.log(fruits); //Avocado and Guava is added at the end of the Array


		//pop()
		/*
		removes the last element in an array AND returns the removed element 

		Syntax:
				arrayName.pop();
		*/

		let removedFruit = fruits.pop();
		console.log(removedFruit);
		console.log('Mutated array from pop method: ');
		console.log(fruits); //Guava is removed


		//Mini Activity

		let ghostFighters = ['Eugene', 'Dennis', 'Alfred', 'Taguro'];

		function unfriend(){
			
			ghostFighters.pop();
			
		}
		unfriend();
		console.log(ghostFighters);

		//unshift()
		/*
		-adds one or more elements at the beginning of our array

		Syntax:
		arrayName.unshift('elementA');
		arrayName.unshift('elementA','elementB');
		*/

		fruits.unshift('Lime','Banana');
		console.log('Mutated array from unshift method: ');
		console.log(fruits);

		//shift()
		/*
		-removes an element at the beginning of an array AND returns the removed element

		Syntax:
		arrayName.shift();
		*/

		let anotherFruit = fruits.shift();
		console.log(anotherFruit); //lime
		console.log('Mutated array from shift method: ');
		console.log(fruits); //the lime is removed in an array

		//splice()
		/*
		simultaneously removes an element from a specified index number and add elements

		Syntax:
		arrayName.splice(startingIndex,deleteCount,elementsToBeAdded)
		*/

		fruits.splice(1,2,'Lime','Cherry'); //can be added as much as you like on the element
		console.log('Mutated array from splice method: ');
		console.log(fruits);

		//sort
		/*
		rearranges the array elements in alphanumeric order
		Syntax:
		arrayName.sort();
		*/
		fruits.sort();
		console.log('Mutated array from sort method: ');
		console.log(fruits);

		//reverse
		/*
		reverses the order of array elements
		Syntax:
		arrayName.reverse();
		*/

		fruits.reverse();
		console.log('Mutated array from reverse method: ');
		console.log(fruits);

		//non-mutatorMethods

		/*
		Non-mutator methods are functions that do not modify or changed an array after they are created
		-these methods do not manipulate the original array performing various task such as returning elements from an array and also combining arrays and printing the output
		*/

		let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];

		//indexOf()

		/*
		returns the index number of the first matching element found in an array
		-if no match is found, the result is -1
		-the search process will be done from first element processing to the last element

		Syntax
		arrayName/indexOf(searchValue);
		arrayName/indexOf(searchValue,fromIndex);
		*/

		let firstIndex = countries.indexOf('PH', 3);
		console.log('Result of IndexOf method: ' + firstIndex); //5 it gets the first matching element

		let inValidCountry = countries.indexOf('BR');
		console.log('Result of IndexOf method: ' + inValidCountry); //-1

		//lastIndexOf()
		/*
		returns the index number of the last matching element found in our array
		-the search process will be done from the last element proceeding to the first element

		Syntax:
		arrayName.lastIndexOf(searchValue);
		arrayName.lastIndexOf(searchValue,fromIndex);
		*/

		let lastIndex = countries.lastIndexOf('PH',3);
		console.log('Result of lastIndexOf method: ' + lastIndex); //result 1

		//slice()
		/*
		-portions/slices elements from an array AND returns a new array

		Syntax:
		arrayName.slice(startingIndex);
		arrayName.slice(startingIndex,endingIndex);
		*/

		let slicedArrayA = countries.slice(2);
		console.log('Result of sliced method: ');
		console.log(slicedArrayA); //the result shows CAN to DE

		let slicedArrayB = countries.slice(2,4);
		console.log('Result of sliced method: ');
		console.log(slicedArrayB) //'CAN', 'SG'

		let slicedArrayC = countries.slice(-3); //starts on the end
		console.log('Result of sliced method: ');
		console.log(slicedArrayC) //'PH', 'FR', 'DE'

		//toString()
		/*
		returns an array as a string separated by commas

		Syntax
		arrayName.toString();
		*/		

		let stringArray = countries.toString();
		console.log('Result of toString method: ');
		console.log(stringArray); //result US,PH,CAN,SG,TH,PH,FR,DE

		//concat()
		/*
		combines two arrays and returns the combined result

		Syntax:
		arrayA.concat(arrayB);
		arrayA.concat(elementA);
		*/

		let taskArrayA = ['drink html', 'eat JavaScript'];
		let taskArrayB = ['inhale css', 'breath sass'];
		let taskArrayC = ['get git', 'be node'];

		let tasks = taskArrayA.concat(taskArrayB);
		console.log('Result from Concat method: ');
		console.log(tasks); //result ['drink html', 'eat JavaScript', 'inhale css', 'breath sass']

		//combine multiple arrays

		console.log('Result from Concat method: ');
		let allTasks = taskArrayA.concat(taskArrayB,taskArrayC);
		console.log(allTasks);

		//Combining arrays with elements
		let combinedTasks = taskArrayA.concat('smell express','throw react');
		console.log('Result from Concat method: ');
		console.log(combinedTasks);

		//join()
		/*
		-returns an array as a string separated by a specified separator by specified separator string
		Syntax:
		arrayName.join('separatorString')
		*/

		let users = ['John','Jane','Joe','Robert','Nej'];
		console.log(users.join());
		console.log(users.join(''));
		console.log(users.join(' - '));

		//Iteration method

		/*
		Iteration methods are loops designed to perform repetitive tasks on arrays
		Iteration methods loops over all items in an array
		Useful for manipulating array data resulting in complex tasks
		*/

		//forEach
		/*
		Similar to a for loop that iterates on each array elemet
		-for each item in the array, the anonymous function passed in the forEach() method will be run
		-the anonymous function is able to receive a current item being iterated or loop over by assigning a parameter
		-variable names for arrays are normally written in the plural from of the data stored in an array
		-It's common practice to use the singular form of the array content for parameter names used in array loops
		-forEach() does NOT return anything

		Syntax:
		arrayName.forEach(Function(indivElement){
			statement
		})
		*/

		allTasks.forEach(function(task){
			console.log(task);
		})

		//Mini Activity
		function ghostDisplay(){
			ghostFighters.forEach(function(ghost){
				console.log(ghost);
			})
		}
		ghostDisplay();

		//Using forEach with conditional statements

		//Looping through all array items
		/*
		It is a good practice to print the current element in the console when working with array iteration methods to have an idea of what information is being worked on for each iteration of the loop

		Creating a separate variable to store results of an array iteration methods are also good practice to avoid confusion by modifying the original array

		Mastering Loops and arrays allow us developers to perform a wide range of features that help in data management and analysis
		*/

		let filteredTasks = [];

		allTasks.forEach(function(task){
			console.log(tasks);
			if(task.length>10){
				console.log(task);
				filteredTasks.push(task)
			}
		});

		console.log('Result of filtered tasks: ');
		console.log(filteredTasks);

		//map()
		/*
		-map iterates on each element AND returns new array with different values depending on the result of the function's operation

		-require a return statement

		Syntax:
		let/const resultArray = arrayName.map(function(indivElement))
		*/

		let numbers = [1,2,3,4,5];

		let numberMap = numbers.map(function(number){
			return number * number;
		});
		console.log("Original Array: ");
		console.log(numbers);
		console.log("Result of Map method: ");
		console.log(numberMap);

		//map() vs forEach()

		let numberForEach = numbers.forEach(function(number){
			return number * number;
		})

		console.log(numberForEach);//forEach does not return newArray

		//every()
		/*
		-checks if all elements in an array meet the given conditions 
		-this is useful for validatin data stored in arrays especially when dealing with large amounts of data
		-returns true if all elements meet the condition, otherwise false 

		Syntax:
		let/const resultArray = arrayName.every(function(indivElement){
			return expression/condition;

		})
		*/

		let allValid = numbers.every(function(number){
			return (number<3);
		});
		console.log('Result of every method: ');
		console.log(allValid); 

		//some()
		/*
		checks if atleast 1 element in the array meets the given condition
		-returns a true value if at least 1 element meets the condition and false if otherwise

		Syntax:
		let/const resultArray = arrayName.some(function(indivElement){
			return expression/condition;
		*/

		let someValid = numbers.some(function(number){
			return (number<2);
		});
		console.log("Result of some method: ");
		console.log(someValid)//true

		//Combining the returned from every/some method may be used in other statements to perform consecutive results
		if (someValid){
			console.log('Some numbers in the array are greater than 2');
		}

		//filter
		/*
		returns a new array that contains elements which meets a given condition 
		-returns an empty array if no elements were found 

		Syntax:
		let/const resultArray = arrayName.filter(function(indivElement){
			return expression/condition
		})
		*/

		let filterValid = numbers.filter(function(number){
			return(number<3);
		})
		console.log('Result of filtered method: ');
		console.log(filterValid); //[1,2]

		let nothingFound = numbers.filter(function(number){
			return (number = 0);
		})
		console.log("result of filter method");
		console.log(nothingFound); //empty array

		//Filtering using forEach
		/*
		
		*/

		let filteredNumbers = [];

		numbers.forEach(function(number){
			/*console.log(number);*/

			if(number<3){
				filteredNumbers.push(number);
			}
		})
		console.log("Result of filter method: ");
		console.log(filteredNumbers);

		//includes()
		/*
		checks if the argument passed can be found in the array
		-returns a boolean which can also be saved in a variable
		-returns true if the argument is found in the array 
		-return false if it is not

		Syntax:

		arrayName.includes(<argumentToFind>)
		*/

		let products = ['Mouse', 'Keyboard','Laptop', 'Monitor'];

		let productFound = products.includes('Mouse');
		console.log(productFound);

		let productNotFound = products.includes('Headset');
		console.log(productNotFound);

		//method chaining

		//methods can be changed by using them one after another
		//The result of the first method is used on the 2nd method until all "chained" methods have been resolved

		let filteredProducts = products.filter(function(product){
			return product.toLowerCase().includes('a');
		})

		console.log(filteredProducts);

		//Mini Activity

		let contacts = ['Ash'];

		function addTrainer(trainer){

			let doesTrainerExists = contacts.includes(trainer);
			if(doesTrainerExists){
				alert("Already added in the Match Call.");
			}else{
				contacts.push(trainer);
				alert('Registered!');
			}
		}

		//reduce
		/*
		-evaluates elements from left to right and returns/reduces the array into a single value

		Syntax
		let/const resultArray = arrayName.reduce(function(accumulator,currentValue){
			return expression/operation
		})

		"Accumulator" parameter in the function stores the result for every iteration of the loop
		'currentValue' is the current/next element in the array that is evaluated in each iteration of the loop

		How the reduce method works:
		1. the first/result element in the array is stored in accumulator parameter
		2. the second/next element in the array is stored in the currentValue parameter
		3. an operation is performed on the two elements 
		4. the loop repeats step 1-3 until all elements have been worked on
		*/	

		console.log(numbers);
		let iteration = 0;
		let iterationStr = 0;

		let reducedArray = numbers.reduce(function(x,y){
			console.warn('current iteration: ' + ++iteration);
			console.log('Accumulator: ' + x);
			console.log('current value: '+ y);

			return x+y;
		})
		console.log("Result of reduce method: " + reducedArray); //15

		let list = ["Hello", 'Again', 'World'];

		let reducedJoin = list.reduce(function(x,y){
			console.warn('current iteration: ' + ++iterationStr);
			console.log('Accumulator: ' + x);
			console.log('current value: '+ y);

			return x + ' ' + y;
		})
		console.log("Result of reduce method: " + reducedJoin);